#!/usr/bin/env python3

def troublesort(v):
    vs = list(v)
    done = False
    while not done:
        done = True
        for i in range(len(vs) - 2):
            if vs[i] > vs[i + 2]:
                done = False
                vs[i], vs[i + 2] = vs[i + 2], vs[i]
    return vs

t = int(input())
for i in range(1, t + 1):
    n = int(input())
    v = [int(vi) for vi in input().split(" ")]

    o = "OK"
    vs = troublesort(v)
    for j in range(len(vs) - 1):
        if vs[j] > vs[j + 1]:
            o = j
            break

    print("Case #{}: {}".format(i, o))
