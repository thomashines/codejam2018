#!/usr/bin/env python3

import math
import sys

gw, gh = 1000, 1000
relatives = tuple((x, y) for x in range(-1, 2) for y in range(-1, 2))

# Functions to get best dimensions to target

def bestdimsa(a):
    # Get square root
    sqrt = a ** 0.5

    # Try combinations
    wh = (
        (int(math.floor(sqrt)), int(math.floor(sqrt))),
        (int(math.floor(sqrt)), int( math.ceil(sqrt))),
        (int( math.ceil(sqrt)), int(math.floor(sqrt))),
        (int( math.ceil(sqrt)), int( math.ceil(sqrt))),
    )
    ww = gw
    hh = gh
    aaa = ww * hh
    for w, h in wh:
        w = max(3, w)
        h = max(3, h)
        aa = w * h
        if aa >= a and aa < aaa:
            ww = w
            hh = h
            aaa = aa

    return ww, hh

def bestdimsb(ta):
    ww = gw
    hh = gh
    aa = ww * hh
    for w in range(3, min(max(4, math.ceil(ta / 3)), gw)):
        h = min(math.ceil(ta / w), gh)
        a = w * h
        if a >= ta and (a < aa or (a == aa and abs(w - h) < abs(ww - hh))):
            ww = w
            hh = h
            aa = a
    return ww, hh


t = int(input())
for i in range(1, t + 1):
    a = int(input())
    # print(i, a, file=sys.stderr)

    # Get the target dimensions
    ww, hh = bestdimsb(a)
    aa = ww * hh
    # print(i, ww, hh, aa, file=sys.stderr)

    # Get the limits
    minx = max(0, math.floor(gw / 2) - math.floor(ww / 2))
    maxx = min(gw - 1, minx + ww - 1)
    miny = max(0, math.floor(gh / 2) - math.floor(hh / 2))
    maxy = min(gh - 1, miny + hh - 1)

    grid = set()

    # print(i, minx, maxx, maxx - minx, file=sys.stderr)
    # print(i, miny, maxy, maxy - miny, file=sys.stderr)

    # Go through rectangle, find the first cell with the most empties, dig there
    c = 0
    e = 9
    x = minx + 1
    y = miny + 1
    while e > 0:
        # print(i, "e", e, file=sys.stderr)
        # print(i, "grid", grid, file=sys.stderr)
        while y <= maxy - 1:
            # print(i, "y", y, file=sys.stderr)
            while x <= maxy - 1:
                # print(i, "x", x, file=sys.stderr)
                ee = 0
                for dx, dy in relatives:
                    if (x + dx, y + dy) not in grid:
                        ee += 1
                # print(i, "ee", ee, file=sys.stderr)
                if ee == e:
                    # print(i, "x y", "{} {}".format(x, y), file=sys.stderr)
                    print("{} {}".format(x, y))
                    c += 1
                    sys.stdout.flush()
                    inp = input()
                    # print(i, "inp", inp, file=sys.stderr)
                    # print(inp, file=sys.stderr)
                    if inp == "-1 -1":
                        sys.exit(-1)
                    elif inp == "0 0":
                        x = maxx
                        y = maxy
                        e = 0
                    else:
                        grid.add(tuple(int(n) for n in inp.split(" ")))
                        # print(i, "grid", grid, file=sys.stderr)
                else:
                    x += 1
                # sys.exit(-1)
            y += 1
            x = minx + 1
        e -= 1
        y = miny + 1
    # print(i, "done grid", grid, file=sys.stderr)
    # print(i, "done a c", a, c, file=sys.stderr)
