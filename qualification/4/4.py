#!/usr/bin/env python3

import math

s = 1
faces = (
    ( # xy
        (-s / 2, -s / 2, -s / 2),
        (-s / 2, +s / 2, -s / 2),
        (+s / 2, -s / 2, -s / 2),
        (+s / 2, +s / 2, -s / 2),
    ),
    ( # yz
        (-s / 2, -s / 2, -s / 2),
        (-s / 2, -s / 2, +s / 2),
        (-s / 2, +s / 2, -s / 2),
        (-s / 2, +s / 2, +s / 2),
    ),
    ( # xz
        (-s / 2, -s / 2, -s / 2),
        (-s / 2, -s / 2, +s / 2),
        (+s / 2, -s / 2, -s / 2),
        (+s / 2, -s / 2, +s / 2),
    ),
)

def dist_xz(a, b):
    return ((b[0] - a[0]) ** 2 + (b[2] - a[2]) ** 2) ** 0.5

def trip_area_xz(a, b, c):
    ab = dist_xz(a, b)
    bc = dist_xz(b, c)
    ca = dist_xz(c, a)
    s = (ab + bc + ca) / 2
    return (s * abs(s - ab) * abs(s - bc) * abs(s - ca)) ** 0.5

def face_area_xz(face):
    return (trip_area_xz(face[0], face[1], face[2]) +
        trip_area_xz(face[0], face[2], face[3]))

def faces_area_xz(faces):
    return sum(face_area_xz(face) for face in faces)

def point_rotate_2d(point, r):
    return (
        point[0] * math.cos(r) - point[1] * math.sin(r),
        point[0] * math.sin(r) + point[1] * math.cos(r),
    )

def point_rotate(point, r):
    dx, dy, dz = point
    dy, dz = point_rotate_2d((dy, dz), r[0])
    dx, dz = point_rotate_2d((dx, dz), r[1])
    dx, dy = point_rotate_2d((dx, dy), r[2])
    return (dx, dy, dz)

def face_rotate(face, r):
    return tuple(point_rotate(point, r) for point in face)

def faces_rotate(faces, r):
    return tuple(face_rotate(face, r) for face in faces)

def faces_rotate_xz(faces, r):
    return tuple(face_rotate(face, (r, 0, r)) for face in faces)

def faces_rotate_yz(faces, r):
    return tuple(face_rotate(face, (0, math.pi / 4, r)) for face in faces)

def face_center(face):
    return (
        sum(point[0] for point in face) / len(face),
        sum(point[1] for point in face) / len(face),
        sum(point[2] for point in face) / len(face),
    )

def faces_centers(faces):
    return tuple(face_center(face) for face in faces)

approx_dx = 0.0001
def approx_deriv(objective, r):
    o = objective(r)
    # return (
    #     (objective((r[0] + approx_dx, r[1], r[2])) - o) / approx_dx,
    #     (objective((r[0], r[1] + approx_dx, r[2])) - o) / approx_dx,
    #     (objective((r[0], r[1], r[2] + approx_dx)) - o) / approx_dx,
    # )
    return (objective(r + approx_dx) - o) / approx_dx

precision = 10 ** -6
gamma = approx_dx * 50
# gamma = approx_dx * 0.1
# minmul = approx_dx * 0.5
t = int(input())
for i in range(1, t + 1):
    print("Case #{}:".format(i))
    a = float(input())

    # n = 16
    # for i in range(n):
    #     print("area", faces_area_xz(faces_rotate(faces, (i * math.pi / n, 0, 1 * math.pi / 4))))
    # print("area", faces_area_xz(faces_rotate(faces, (math.pi / 4, math.pi / 4, math.pi / 4))))

    # objective = lambda r: abs(a - faces_area_xz(faces_rotate(faces, r)))
    # objective = lambda r: abs(a - faces_area_xz(faces_rotate_xz(faces, r)))
    # objective = lambda r: abs(a - faces_area_xz(faces_rotate_yz(faces, r)))

    # r = (math.pi / 4, math.pi / 4, math.pi / 4)
    # r = (0, 0, 0)
    # r = math.pi / 4
    # n = 0
    # o = objective(r)
    # while n < 1000 and o > precision:
    #     # mul = max(minmul, o * gamma)
    #     # print(o)
    #     mul = math.sqrt(o) * gamma
    #     # mul = o * gamma
    #     # mul = gamma
    #     do = approx_deriv(objective, r)
    #     # r = (r[0] - mul * do[0], r[1] - mul * do[1], r[2] - mul * do[2])
    #     r = r - mul * do
    #     # r = (r[0] - mul * do[0], r[1], r[2])
    #     o = objective(r)
    #     n += 1

    # r = math.asin(a / 2) - math.pi / 2
    r = 2 * math.atan2(math.sqrt(2) - math.sqrt(3 - a ** 2), a + 1)
    # r = 2 * math.atan2(math.sqrt(3 - a ** 2) + math.sqrt(2), a + 1)

    # print("a", a)
    # print("r", r)
    # print("n", n)
    # print("objective", objective(r))
    # print("precision", precision)
    # print("objective > precision", objective(r) > precision)
    # print("area", faces_area_xz(faces_rotate_xz(faces, r)))
    # print("area", faces_area_xz(faces_rotate_yz(faces, r)))

    # out = faces_centers(faces_rotate_xz(faces, r))
    out = faces_centers(faces_rotate_yz(faces, r))
    print(*out[0])
    print(*out[1])
    print(*out[2])
