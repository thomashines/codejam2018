#!/usr/bin/env python3

def damage(p):
    d = 0
    s = 1
    for c in reversed(p):
        if c == "S":
            d += s
        elif c == "C":
            s *= 2
    return d

t = int(input())
for i in range(1, t + 1):
    ds, ps = input().split(" ")
    d = int(ds)
    p = list(reversed(list(ps)))

    # Best strategy is to move last S to the left
    c = 0
    # print(i, p, damage(p))
    while damage(p) > d:
        # Get position of last S
        si = p.index("S")

        # Go left until the first C that can be swapped with an S
        while si < len(p) - 1 and p[si + 1] == "S":
            si += 1

        if si == len(p) - 1:
            # Nothing to do
            c = "IMPOSSIBLE"
            break

        # Swap
        p[si], p[si + 1] = p[si + 1], p[si]
        # p[si - 1:si] = p[si:si - 1]
        c += 1
        # print(i, p, damage(p))

    print("Case #{}: {}".format(i, c))
